//  
//  MhhFirebase.swift
//  MhhFirebase
//
//  Created by Mathias H. Hansen on 13/01/2018.
//
//  Copyright 2017-2018 Mathias Hedemann Hansen.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//


import FirebaseCore
import FirebaseAuth
import MhhCommon


// MARK: Type Aliases

typealias MhhAuth = MhhCommon.Auth  // Firebase also has an Auth
typealias FBAuth = FirebaseAuth.Auth
public typealias FBUser = FirebaseAuth.User
public typealias MhhUser = MhhCommon.User


// MARK: Extensions

// We do this so we can replace FBUser with MhhUser so we have MhhUser as dependency and not FBUser
// Also a MhhUser is not completely the same as a FBUser
extension FBUser: MhhUser {
	public var verificationEmailSentAgainText: String {
		get {
			return "Verification email sent again"
		}
	}
}


// MARK: MhhFirebase

public class MhhFirebase: AuthNetwork {

	// MARK: Properties
	
	// For each of your app's views that need information about the signed-in user, attach a listener to the FIRAuth object. This listener gets called whenever the user's sign-in state changes.
	var handle: AuthStateDidChangeListenerHandle?

	
	// MARK: Init and setup
	
	public init() {}
	
	public func setupFirebase() {
		FirebaseApp.configure()
	}
	
	
	// MARK: Listeners
	
	public func startAuthStateDidChangeListener() {
		// Listen for authentication state
		// For each of your app's views that need information about the signed-in user, attach a listener to the FIRAuth object. This listener gets called whenever the user's sign-in state changes.
		// We don't use this nice functionality at the moment. I am building a general simple lib at the moment without niceness like this
		handle = FBAuth.auth().addStateDidChangeListener { (auth, user) in
		}
	}
	
	public func stopAuthStateDidChangeListener() {
		Auth.auth().removeStateDidChangeListener(handle!)
	}
	
	
	// MARK: Auth protocol conformation
	
	public func login(
		with email: String,
		and password: String,
		callback: @escaping (Error?, MhhUser?) -> Void
	) {
		FBAuth.auth().signIn(withEmail: email, password: password) { (user: FBUser?, error: Error?) in  // Firebase do (user, error). It is opposite of me doing (error, user)

			callback(error, user)
		}
	}
	
	public func logout(callback: ((Error?, Bool) -> Void)? = nil){
		do {
			print("Trying to sign out user")
			_ = try FBAuth.auth().signOut()
			let (isLoggedIn, _) = isUserLoggedIn()
			callback?(nil, isLoggedIn)
			print("user is signed out")
		}
		catch let signOutError as NSError {
			print ("Error signing out: %@", signOutError)
			let (isLoggedIn, _) = isUserLoggedIn()
			callback?(signOutError, isLoggedIn)
		}
	}
	
	public func resetEmail(
		email: String,
		callback: ((Error?) -> Void)? = nil
	) {
		FBAuth.auth().sendPasswordReset(withEmail: email) { (error: Error?) in
			callback?(error)
		}
	}
	
	
	// MARK: AuthNetwork protocol conformation
	
	public func createUser(
		with email: String,
		and password: String,
		callback: @escaping (Error?, MhhUser?) -> Void) {
		FBAuth.auth().createUser(
		  withEmail: email,
			password: password) {
				(user: MhhUser?, error: Error?) in
			self.printErrorOrSuccess(error, user, str: "Firebase")
		  // Firebase do (user, error).
			// It is opposite of me doing (error, user)
			callback(error, user)
		}
	}

	public func sendVerificationEmail(user: MhhUser) {
		printUser(user: user)
		user.sendEmailVerification(completion: { (error: Error?) in
			if let error = error {
				print("Error \(error) when sending verification email")
			} else {
				print("verification email was sent")
			}
		})
	}
	
	public func changeEmail(
		newEmail: String,
		callback: (Error?) -> Void
	) {
		assertionFailure("Mhh says: This is not implented yet")
		// TODO: mhh. implement this functionality
		// TODO: mhh. todo try to user and newEmail that another user already have registered with.
//		FBAuth.auth().currentUser?.updateEmail(to: <#T##String#>, completion: <#T##UserProfileChangeCallback?##UserProfileChangeCallback?##(Error?) -> Void#>)
	}

	// MARK: Not in protocol

	public func isUserLoggedIn() -> (Bool, FBUser?) {
		return isUserLoggedIn(auth: FBAuth.auth())
	}
	
	// TODO: mhh. Should be in protocol. the protocol should be func isUserLoggedIn(auth: Any)
	public func isUserLoggedIn(auth: Any) -> (Bool, FBUser?) {
		let fbAuth = auth as! FBAuth
		if let currentUser = fbAuth.currentUser {
			printUser(user: currentUser)
			return (true, currentUser)
		} else {
			print("No user is signed in.")
			return (false, nil)
		}
	}

	
	// MARK: Print
	
	public func printErrorOrSuccess(_ error: Error?, _ user: MhhUser?, str: String) {
		if let error = error {
			print("\(str). my error \(error) not created")
		}
		if let user = user {
			print("\(str). my user \(user) created")
		}
	}
	
	func printUser(user: MhhUser, withText: String = "is logged in") {
		print("""
			
			--------------- start -------------------
			
			User \(user) with UID \(user.uid) and email  \(user.email) \(withText). The users email is verified: \(user.isEmailVerified)
			
			----------------- end -------------------
			
			""")
	}
	
	func printError(error: Error, prefix: String = "", postfix: String = "") {
		print("""
			
			--------------- start -------------------
			
			\(prefix). The error: \(error) \(postfix)
			
			----------------- end -------------------
			
			""")
	}
	
	func printMe(stuff: Any, prefix: String = "", postfix: String = "") {
		print("""
			
			--------------- start -------------------

			\(prefix). Stuff is: \(stuff) \(postfix)
			
			----------------- end -------------------
			
""")
	}
	
}

