//
//  ViewController.swift
//  MhhFirebase
//
//  Created by Mathias H. Hansen on 01/13/2018.
//  Copyright (c) 2018 Mathias H. Hansen. All rights reserved.
//

import UIKit
import MhhFirebase

class ViewController: UIViewController {
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		let email = "johndoe@email.com"
		let password = "123456"
		//			MhhFirebase().signIn(with: email, and: password, callBack: nil)
	
		MhhFirebase().login(with: email, and: password) { (error: Error?, user: Any?) in
			if let error = error {
				print("Example my error \(error)")
			}
			if let user = user {
				print("Example my user \(user)")
			}
		}
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
}

