# MhhFirebase

[![CI Status](http://img.shields.io/travis/Mathias H. Hansen/MhhFirebase.svg?style=flat)](https://travis-ci.org/Mathias H. Hansen/MhhFirebase)
[![Version](https://img.shields.io/cocoapods/v/MhhFirebase.svg?style=flat)](http://cocoapods.org/pods/MhhFirebase)
[![License](https://img.shields.io/cocoapods/l/MhhFirebase.svg?style=flat)](http://cocoapods.org/pods/MhhFirebase)
[![Platform](https://img.shields.io/cocoapods/p/MhhFirebase.svg?style=flat)](http://cocoapods.org/pods/MhhFirebase)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

MhhFirebase is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'MhhFirebase'
```

## Author

Mathias H. Hansen, dev@mathiashh.dk

## License

MhhFirebase is available under the MIT license. See the LICENSE file for more info.
